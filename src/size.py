class Size:

    def __init__(self, width, height):

        self.axis = (width,height)

        self.mid = (width // 2, height // 2)

        self.net = width // 140

        self.pos_y = height / 2.5

        self.r_pos_x = height // 25

        self.l_pos_x = width - 2*(height // 25) 

        self.paddle = (width // 70, height // 5)

        self.ball = ((width*height)**0.5)//(height//10)

        self.up_border_b =  self.ball

        self.down_border_b =  height - self.ball

        self.up_border_p =  0

        self.down_border_p =  height - self.paddle[1]

        self.r_border = 0

        self.l_border = width - self.ball

