# Import the pygame library and initialise the game engine
import pygame
# Import the Paddle Class
from paddle import Paddle
# Import the Ball Class
from ball import Ball
# Import the Size Class
from size import Size

pygame.init()

# Define some colors
BLACK = (0,0,0)
WHITE = (255,255,255)

# Open a new window
size = Size(700,500)
screen = pygame.display.set_mode(size.axis)
pygame.display.set_caption("Pong")

# Create the paddle/objects
paddleA = Paddle(WHITE, size.paddle[0], size.paddle[1])
paddleA.rect.x = size.r_pos_x
paddleA.rect.y = size.pos_y

paddleB = Paddle(WHITE, size.paddle[0], size.paddle[1])
paddleB.rect.x = size.l_pos_x
paddleB.rect.y = size.pos_y

# Create the ball instance
ball = Ball(WHITE, size.ball, size.ball)
ball.rect.x = size.mid[0]
ball.rect.y = size.mid[1]

# Create the list that contains all the sprites used in the game
all_sprites_list = pygame.sprite.Group()

# Add the paddles to the list of sprites
all_sprites_list.add(paddleA)
all_sprites_list.add(paddleB)
all_sprites_list.add(ball)

# The loop will carry on until the user exit the game
carryOn = True

# The clock will be used to control how fast the screen updates
clock = pygame.time.Clock()

# ----------- Main Program Loop ------------
while carryOn:
    # --- Main event loop
    for event in pygame.event.get(): # User did something
        if (event.type == pygame.QUIT or 
            (event.type == pygame.KEYDOWN and event.key == pygame.K_q)): # If user clicked close
                carryOn = False # Flag that we are done so exit this loop
        if (event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE):
            ball.start()

    # Moving the paddles when pressing the arrow keys (paddleA) or "W/S" keys (paddleB)
    keys = pygame.key.get_pressed()
    if keys[pygame.K_w]:
        paddleA.moveUp(5, size.up_border_p)
    if keys[pygame.K_s]:
        paddleA.moveDown(5, size.down_border_p)
    if keys[pygame.K_UP]:
        paddleB.moveUp(5, size.up_border_p)
    if keys[pygame.K_DOWN]:
        paddleB.moveDown(5, size.down_border_p)


    # --- Game logic
    all_sprites_list.update()

    # Check if the ball is bouncing against any of the 4 walls:
    if ball.rect.x >= size.l_border or ball.rect.x <= size.r_border:
        ball.velocity[0] = -ball.velocity[0]
    if ball.rect.y > size.down_border_b or ball.rect.y < size.up_border_b:
        ball.velocity[1] = -ball.velocity[1]

    # Detect collisions between the ball and the paddles
    if pygame.sprite.collide_mask(ball, paddleA) or pygame.sprite.collide_mask(ball, paddleB):
        ball.bounce()


    # --- Drawing code
    # First, clear the screen to black.
    screen.fill(BLACK)
    # Draw the net
    pygame.draw.line(screen, WHITE, [size.mid[0], 0],[size.mid[0], size.axis[1]], size.net)

    # Draw all the sprites
    all_sprites_list.draw(screen)

    # --- Go ahead and update the screen with what we've drawn
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Once we have exited the main program loop we can stop the game engine:
pygame.quit()

