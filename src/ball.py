import pygame
from random import randint
BLACK = (0,0,0)

class Ball(pygame.sprite.Sprite):
    # This class represents a ball. It derives from the "Sprite" class in Pygame.

    def __init__(self, color, width, height):
        # Call the parent class(Sprite) constructor
        super().__init__()

        # Pass in the color of the ball, and its x and y position, width and height.
        # Set the background color and set it to be transparent.
        self.image = pygame.Surface([width, height])
        self.image.fill(BLACK)
        self.image.set_colorkey(BLACK)

        # Draw the ball.
        pygame.draw.rect(self.image, color, [0, 0, width, height])

        # Only start to bounce if an event occurs
        self.allow_mov = False
        self.velocity = [0,0]

        # Fetch te rectangle object that has the dimensions of the image.
        self.rect = self.image.get_rect()

    def start(self):
        self.allow_mov = True
        self.velocity = [randint(4,8), randint(-8,8)]

    def update(self):
        if self.allow_mov:
            self.rect.x += self.velocity[0]
            self.rect.y += self.velocity[1]

    def bounce(self):
        self.velocity[0] = -self.velocity[0]
        self.velocity[1] = randint(-8,8)
